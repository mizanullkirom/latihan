package person

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"go.mongodb.org/mongo-driver/bson"
)

type PersonHandler struct{}

func (ph *PersonHandler) GetPerson(w http.ResponseWriter, r *http.Request) {
	nik := chi.URLParam(r, "nik")
	if len(nik) == 0 {
		http.Error(w, fmt.Sprintf("nik must required"), 400)
		return
	}
	person := &Person{}
	err := GetOne(person, bson.M{"nik": nik})
	if err != nil {
		http.Error(w, fmt.Sprintf("person not found"), 404)
		return
	}

	json.NewEncoder(w).Encode(person)
}

func (ph *PersonHandler) AddPerson(w http.ResponseWriter, r *http.Request) {
	existingPerson := &Person{}
	var person Person
	json.NewDecoder(r.Body).Decode(&person)
	if &person.NIK == nil || person.NIK == "" {
		http.Error(w, fmt.Sprintf("nik must required"), 422)
		return
	}
	err := GetOne(existingPerson, bson.M{"nik": person.NIK})
	if err == nil {
		http.Error(w, fmt.Sprintf("person already exist"), 400)
		return
	}
	_, err = AddOne(&person)
	if err != nil {
		http.Error(w, fmt.Sprint(err), 400)
		return
	}
	w.Write([]byte("person created successfully"))
	w.WriteHeader(201)
}
