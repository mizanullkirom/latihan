package person

import (
	"context"
	"time"

	"gitlab.com/mizanullkirom/latihan/common"
	"go.mongodb.org/mongo-driver/mongo"
)

type Person struct {
	Name        string `json:"name" bson:"name"`
	NIK         string `json:"nik" bson:"nik"`
	PhoneNumber string `json:"phoneNumber" bson:"phoneNumber"`
	Address     string `json:"address" bson:"address"`
}

func GetOne(c *Person, filter interface{}) error {

	//Will automatically create a collection if not available
	mdb := common.GetMongo()
	collection := mdb.Collection("person")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	err := collection.FindOne(ctx, filter).Decode(c)
	return err
}

func AddOne(p *Person) (*mongo.InsertOneResult, error) {
	mdb := common.GetMongo()
	collection := mdb.Collection("person")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result, err := collection.InsertOne(ctx, p)
	return result, err
}

func Update(filter interface{}, update interface{}) (*mongo.UpdateResult, error) {
	mdb := common.GetMongo()
	collection := mdb.Collection("person")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result, err := collection.UpdateMany(ctx, filter, update)
	return result, err
}

func RemoveOne(filter interface{}) (*mongo.DeleteResult, error) {
	mdb := common.GetMongo()
	collection := mdb.Collection("person")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	result, err := collection.DeleteOne(ctx, filter)
	return result, err

}
