package person_test

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi/v5"
	"github.com/stretchr/testify/assert"
	"gitlab.com/mizanullkirom/latihan/common"
	p "gitlab.com/mizanullkirom/latihan/person"
)

func TestAddPerson(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit()

	var reqTest = []struct {
		bodyData     string
		expectedCode int
	}{
		{
			`{"name": "mizan","nik": "1103130066","phoneNumber" : "007", "address" : "tegalCity"}`,
			200,
		},
		{
			`{"name": "mizan","nik": "1103130066","phoneNumber" : "007", "address" : "tegalCity"}`,
			400,
		},
		{
			`{"name": "mizan","nik": "","phoneNumber" : "007", "address" : "tegalCity"}`,
			422,
		},
	}

	r := chi.NewRouter()
	ph := &p.PersonHandler{}
	r.Post("/person", ph.AddPerson)

	for _, testData := range reqTest {
		bodyData := testData.bodyData
		req, err := http.NewRequest("POST", "/person", bytes.NewBufferString(bodyData))
		req.Header.Set("Content-Type", "application/json")
		asserts.NoError(err)

		w := httptest.NewRecorder()
		r.ServeHTTP(w, req)

		asserts.Equal(testData.expectedCode, w.Code, "Respon")
	}
}

func TestGetPerson(t *testing.T) {
	asserts := assert.New(t)
	common.MongoInit()

	var reqTest = []struct {
		nik          string
		expectedCode int
	}{
		{"1103130066", 200},
		{"066", 404},
		{"", 400},
	}

	r := chi.NewRouter()
	ph := &p.PersonHandler{}
	r.Get("/person/{nik}", ph.GetPerson)

	for _, testData := range reqTest {
		nik := testData.nik
		req, err := http.NewRequest("GET", fmt.Sprintf("/person/%s", nik), nil)
		req.Header.Set("Content-Type", "application/json")
		asserts.NoError(err)

		w := httptest.NewRecorder()
		r.ServeHTTP(w, req)

		asserts.Equal(testData.expectedCode, w.Code, "Response")
	}
}
