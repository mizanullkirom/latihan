package common

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const DefaultDatabase = "test"

type MongoConn struct {
	client   *mongo.Client
	database string
}

//NewHandler is MongoHandler Constructor
func NewHandler(address string) *MongoConn {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	cl, _ := mongo.Connect(ctx, options.Client().ApplyURI(address))
	mh := &MongoConn{
		client:   cl,
		database: DefaultDatabase,
	}
	return mh
}

var mh *MongoConn

//MongoInit for init mongodb connection
func MongoInit() *MongoConn {
	mongoDbConnection := "mongodb://127.0.0.1:27017"
	mh = NewHandler(mongoDbConnection)
	return mh
}

//GetMongo for get mongodb connection
func GetMongo() *mongo.Database {
	return mh.client.Database(mh.database)
}
